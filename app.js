let express = require('express')
let app = express();
let bodyParser = require('body-parser');
let morgan = require('morgan')
let server_port = process.env.PORT || 5678;
let path = require('path');
cors = require('cors'),
  app.use(cors());
app.use(morgan('dev'))
app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
/*.................attach a front end website..................*/
app.use(express.static(path.join(__dirname, 'public/')));
app.get('*', function (req, res) {  //always use * b refresh time it gives error..
  res.sendFile(__dirname + '/public/index.html');
});
/*.................attach a front end website..................*/
app.listen(server_port, '0.0.0.0', () => {
  console.log("Web App Connected on port ==>", server_port)
});