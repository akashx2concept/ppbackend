import { ApplicationConfig, ScanningApplication } from './application';
export * from './application';
export declare function main(options?: ApplicationConfig): Promise<ScanningApplication>;
