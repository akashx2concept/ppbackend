import { Entity } from '@loopback/repository';
export declare class Images extends Entity {
    id?: string;
    url: string;
    originalName: string;
    displayName: string;
    type: string;
    docType: string;
    createdAt?: string;
    updatedAt?: string;
    userId?: string;
    createdBy?: string;
    isFolder: boolean;
    isFolderId?: number;
    constructor(data?: Partial<Images>);
}
export interface ImagesRelations {
}
export declare type ImagesWithRelations = Images & ImagesRelations;
