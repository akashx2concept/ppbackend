import { Entity } from '@loopback/repository';
export declare class Container extends Entity {
    name: string;
    constructor(data?: Partial<Container>);
}
