import { Entity } from '@loopback/repository';
export declare class Role extends Entity {
    id?: number;
    name: string;
    slug: string;
    isDefault?: boolean;
    isActive?: boolean;
    isDeleted?: boolean;
    createdAt?: string;
    updatedAt?: string;
    constructor(data?: Partial<Role>);
}
export interface RoleRelations {
}
export declare type RoleWithRelations = Role & RoleRelations;
