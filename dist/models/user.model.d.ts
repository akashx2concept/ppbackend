import { Entity } from '@loopback/repository';
import { UserCredentials } from './user-credentials.model';
export declare class User extends Entity {
    id?: string;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    email: string;
    userCredentials: UserCredentials;
    resetKey?: string;
    resetCount: number;
    resetTimestamp: string;
    resetKeyTimestamp: string;
    createdAt?: string;
    updatedAt?: string;
    roleId?: number;
    roles?: string[];
    createdBy?: number;
    isActive?: boolean;
    isDeleted?: boolean;
    constructor(data?: Partial<User>);
}
export interface UserRelations {
}
export declare type UserWithRelations = User & UserRelations;
