"use strict";
// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefreshTokenServiceBindings = exports.TokenServiceBindings = exports.GlobalConfigBinding = exports.UserServiceBindings = exports.PasswordHasherBindings = void 0;
const context_1 = require("@loopback/context");
var PasswordHasherBindings;
(function (PasswordHasherBindings) {
    PasswordHasherBindings.PASSWORD_HASHER = context_1.BindingKey.create('services.hasher');
    PasswordHasherBindings.ROUNDS = context_1.BindingKey.create('services.hasher.round');
})(PasswordHasherBindings = exports.PasswordHasherBindings || (exports.PasswordHasherBindings = {}));
var UserServiceBindings;
(function (UserServiceBindings) {
    UserServiceBindings.USER_SERVICE = context_1.BindingKey.create('services.user.service');
})(UserServiceBindings = exports.UserServiceBindings || (exports.UserServiceBindings = {}));
var GlobalConfigBinding;
(function (GlobalConfigBinding) {
    GlobalConfigBinding.Global_Config = context_1.BindingKey.create('service.globalConfig');
})(GlobalConfigBinding = exports.GlobalConfigBinding || (exports.GlobalConfigBinding = {}));
var TokenServiceBindings;
(function (TokenServiceBindings) {
    TokenServiceBindings.TOKEN_SECRET = context_1.BindingKey.create('authentication.jwt.secret');
    TokenServiceBindings.TOKEN_EXPIRES_IN = context_1.BindingKey.create('authentication.jwt.expires.in.seconds');
    TokenServiceBindings.TOKEN_SERVICE = context_1.BindingKey.create('services.authentication.jwt.tokenservice');
})(TokenServiceBindings = exports.TokenServiceBindings || (exports.TokenServiceBindings = {}));
var RefreshTokenServiceBindings;
(function (RefreshTokenServiceBindings) {
    RefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE = context_1.BindingKey.create('services.authentication.jwt.refresh.tokenservice');
    RefreshTokenServiceBindings.REFRESH_SECRET = context_1.BindingKey.create('authentication.jwt.refresh.secret');
    RefreshTokenServiceBindings.REFRESH_EXPIRES_IN = context_1.BindingKey.create('authentication.jwt.refresh.expires.in.seconds');
    RefreshTokenServiceBindings.REFRESH_ISSUER = context_1.BindingKey.create('authentication.jwt.refresh.issuer');
    /**
     * The backend datasource for refresh token's persistency.
     */
    RefreshTokenServiceBindings.DATASOURCE_NAME = 'refreshdb';
    /**
     * Key for the repository that stores the refresh token and its bound user
     * information
     */
    RefreshTokenServiceBindings.REFRESH_REPOSITORY = 'repositories.RefreshTokenRepository';
})(RefreshTokenServiceBindings = exports.RefreshTokenServiceBindings || (exports.RefreshTokenServiceBindings = {}));
//# sourceMappingURL=keys.js.map