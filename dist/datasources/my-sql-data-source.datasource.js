"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MySqlDataSourceDataSource = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
// const config = {
//   name: 'MySqlDataSource',
//   connector: 'mysql',
//   url: '',
//   host: 'localhost',
//   port: 3306,
//   user: 'root',
//   password: 'Admin@123',
//   database: 'ppScanningQA'
// };
const config = {
    name: 'MySqlDataSource',
    connector: 'mysql',
    url: '',
    host: 'ls-2529ec5900ad5fc2cb01e06f551e79d7b40814ef.ch8dfjvpaoc5.us-east-1.rds.amazonaws.com',
    port: 3306,
    user: 'dbmasteruser',
    password: 'R6(bR3?KJq5[&[`yYACN(sI,Z9Yrwon6',
    database: 'ppScanningQA'
};
// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
let MySqlDataSourceDataSource = class MySqlDataSourceDataSource extends repository_1.juggler.DataSource {
    constructor(dsConfig = config) {
        super(dsConfig);
    }
};
MySqlDataSourceDataSource.dataSourceName = 'MySqlDataSource';
MySqlDataSourceDataSource.defaultConfig = config;
MySqlDataSourceDataSource = tslib_1.__decorate([
    core_1.lifeCycleObserver('datasource'),
    tslib_1.__param(0, core_1.inject('datasources.config.MySqlDataSource', { optional: true })),
    tslib_1.__metadata("design:paramtypes", [Object])
], MySqlDataSourceDataSource);
exports.MySqlDataSourceDataSource = MySqlDataSourceDataSource;
//# sourceMappingURL=my-sql-data-source.datasource.js.map