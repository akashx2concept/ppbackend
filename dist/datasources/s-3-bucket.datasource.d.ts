import { LifeCycleObserver } from '@loopback/core';
import { juggler } from '@loopback/repository';
export declare class S3BucketDataSource extends juggler.DataSource implements LifeCycleObserver {
    static dataSourceName: string;
    static readonly defaultConfig: {
        name: string;
        connector: string;
        provider: string;
        key: string;
        keyId: string;
        maxFileSize: string;
    };
    constructor(dsConfig?: object);
}
