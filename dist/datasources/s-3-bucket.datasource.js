"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.S3BucketDataSource = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const config = {
    name: 's3Bucket',
    connector: 'loopback-component-storage',
    provider: "amazon",
    key: "vz+PX5r0sPOerG2Ic3NS/YFiZ4bNF/thzTJyoqw0",
    keyId: "AKIAV3XFHJZRHTS2NV54",
    maxFileSize: "52428800"
};
// const config = {
//   name: 's3Bucket',
//   connector: 'loopback-component-storage',
//   provider: "amazon",
//   key: "hiBu8t3N84JIgPZsVWF65+B5cDYNDHUmmTc50shD",
//   keyId: "AKIATYIJB3T2JQ3DZBVO",
//   maxFileSize: "52428800"
// };
// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
let S3BucketDataSource = class S3BucketDataSource extends repository_1.juggler.DataSource {
    constructor(dsConfig = config) {
        super(dsConfig);
    }
};
S3BucketDataSource.dataSourceName = 's3Bucket';
S3BucketDataSource.defaultConfig = config;
S3BucketDataSource = tslib_1.__decorate([
    core_1.lifeCycleObserver('datasource'),
    tslib_1.__param(0, core_1.inject('datasources.config.s3Bucket', { optional: true })),
    tslib_1.__metadata("design:paramtypes", [Object])
], S3BucketDataSource);
exports.S3BucketDataSource = S3BucketDataSource;
//# sourceMappingURL=s-3-bucket.datasource.js.map