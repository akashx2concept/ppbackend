export declare class GlobalConfigService {
    APPLICATION_URL: string;
    S3Container: any;
    sendGridAutherizationToken: string;
    sendGridSendMailEmailId: string;
    sendGridSendMailUserName: string;
    constructor();
}
