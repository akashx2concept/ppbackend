"use strict";
// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailService = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const keys_1 = require("../keys");
const globalConfig_service_1 = require("./globalConfig.service");
let sgMail = require('@sendgrid/mail');
let EmailService = class EmailService {
    constructor(globalConfigService) {
        this.globalConfigService = globalConfigService;
        sgMail.setApiKey(this.globalConfigService.sendGridAutherizationToken);
        // console.log(sgMail, "sgMail")
    }
    /**
     * If using gmail see https://nodemailer.com/usage/using-gmail/
     */
    // private static async setupTransporter() {
    // return createTransport({
    //   host: process.env.SMTP_SERVER,
    //   port: +process.env.SMTP_PORT!,
    //   secure: false, // upgrade later with STARTTLS
    //   auth: {
    //     user: process.env.SMTP_USERNAME,
    //     pass: process.env.SMTP_PASSWORD,
    //   },
    // });
    // }
    async sendResetPasswordMail(user) {
        const msg = {
            to: user.email,
            from: this.globalConfigService.sendGridSendMailEmailId,
            subject: 'Reset Password Request',
            text: 'and easy to do anywhere, even with Node.js',
            html: `
      <div>
          <p>Hello, ${user.firstName}</p>
          <p style="color: red;">We received a request to reset the password for your account with email address: ${user.email}</p>
          <p>To reset your password click on the link provided below</p>
          <a href="${this.globalConfigService.APPLICATION_URL}/auth/reset-password?resetKey=${user.resetKey}">Reset your password link</a>
          <p>If you didn’t request to reset your password, please ignore this email or reset your password to protect your account.</p>
          <p>Thanks</p>
          <p>PP Scanning</p>
      </div>
      `,
        };
        return (async () => {
            try {
                await sgMail.send(msg);
                return { massage: "success" };
            }
            catch (error) {
                console.error(error);
                if (error.response) {
                    console.error(error.response.body);
                }
            }
        })();
        // const transporter = await EmailService.setupTransporter();
        // const emailTemplate = new EmailTemplate({
        //   to: user.email,
        //   subject: '[Shoppy] Reset Password Request',
        //   html: `
        //   <div>
        //       <p>Hello, ${user.firstName} ${user.lastName}</p>
        //       <p style="color: red;">We received a request to reset the password for your account with email address: ${user.email}</p>
        //       <p>To reset your password click on the link provided below</p>
        //       <a href="${process.env.APPLICATION_URL}/reset-password-finish.html?resetKey=${user.resetKey}">Reset your password link</a>
        //       <p>If you didn’t request to reset your password, please ignore this email or reset your password to protect your account.</p>
        //       <p>Thanks</p>
        //       <p>LoopBack'ers at Shoppy</p>
        //   </div>
        //   `,
        // });
        // return transporter.sendMail(emailTemplate);
    }
};
EmailService = tslib_1.__decorate([
    core_1.bind({ scope: core_1.BindingScope.TRANSIENT }),
    tslib_1.__param(0, core_1.inject(keys_1.GlobalConfigBinding.Global_Config)),
    tslib_1.__metadata("design:paramtypes", [globalConfig_service_1.GlobalConfigService])
], EmailService);
exports.EmailService = EmailService;
//# sourceMappingURL=email.service.js.map