import { User } from '../models';
import { GlobalConfigService } from './globalConfig.service';
export declare class EmailService {
    globalConfigService: GlobalConfigService;
    constructor(globalConfigService: GlobalConfigService);
    /**
     * If using gmail see https://nodemailer.com/usage/using-gmail/
     */
    sendResetPasswordMail(user: User): Promise<any>;
}
