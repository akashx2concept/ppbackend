import { UserService } from '@loopback/authentication';
import { UserProfile } from '@loopback/security';
import { User, UserWithPassword, UserWithRelations } from '../models';
import { Credentials, UserRepository } from '../repositories';
import { EmailService } from './email.service';
import { PasswordHasher } from './hash.password.bcryptjs';
export declare class UserManagementService implements UserService<User, Credentials> {
    userRepository: UserRepository;
    passwordHasher: PasswordHasher;
    emailService: EmailService;
    readonly jwtSecreat: string;
    readonly jwtSecreatKeyExpiresIn: string;
    constructor(userRepository: UserRepository, passwordHasher: PasswordHasher, emailService: EmailService);
    verifyCredentials(credentials: Credentials): Promise<User>;
    convertToUserProfile(user: any): UserProfile;
    findUserById(id: string): Promise<User & UserWithRelations>;
    createUser(userWithPassword: UserWithPassword): Promise<User>;
    requestPasswordReset(email: string): Promise<any>;
    /**
     * Checks user reset timestamp if its same day increase count
     * otherwise set current date as timestamp and start counting
     * For first time reset request set reset count to 1 and assign same day timestamp
     * @param user
     */
    updateResetRequestLimit(user: User): Promise<User>;
    /**
     * Ensures reset key is only valid for a day
     * @param user
     */
    validateResetKeyLifeSpan(user: User): Promise<User>;
    generateRefreshToken(userProfile: any): Promise<any>;
}
