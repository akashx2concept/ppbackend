import { TokenService, UserService } from '@loopback/authentication';
import { RefreshTokenService } from '@loopback/authentication-jwt';
import { BindingKey } from '@loopback/context';
import { User } from './models';
import { Credentials } from './repositories';
import { PasswordHasher } from './services';
import { GlobalConfigService } from './services';
export declare namespace PasswordHasherBindings {
    const PASSWORD_HASHER: BindingKey<PasswordHasher<string>>;
    const ROUNDS: BindingKey<number>;
}
export declare namespace UserServiceBindings {
    const USER_SERVICE: BindingKey<UserService<User, Credentials>>;
}
export declare namespace GlobalConfigBinding {
    const Global_Config: BindingKey<GlobalConfigService>;
}
export declare namespace TokenServiceBindings {
    const TOKEN_SECRET: BindingKey<string>;
    const TOKEN_EXPIRES_IN: BindingKey<string>;
    const TOKEN_SERVICE: BindingKey<TokenService>;
}
export declare namespace RefreshTokenServiceBindings {
    const REFRESH_TOKEN_SERVICE: BindingKey<RefreshTokenService>;
    const REFRESH_SECRET: BindingKey<string>;
    const REFRESH_EXPIRES_IN: BindingKey<string>;
    const REFRESH_ISSUER: BindingKey<string>;
    /**
     * The backend datasource for refresh token's persistency.
     */
    const DATASOURCE_NAME = "refreshdb";
    /**
     * Key for the repository that stores the refresh token and its bound user
     * information
     */
    const REFRESH_REPOSITORY = "repositories.RefreshTokenRepository";
}
