"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
var moment = require('moment-timezone');
let RoleRepository = class RoleRepository extends repository_1.DefaultCrudRepository {
    constructor(dataSource) {
        super(models_1.Role, dataSource);
    }
    definePersistedModel(entityClass) {
        const modelClass = super.definePersistedModel(entityClass);
        modelClass.observe('before save', async (ctx) => {
            // console.log(`going to save ${ctx.Model.modelName}`);
            if (ctx.instance && !ctx.instance.id) {
                ctx.instance.createdAt = moment().format('x');
                ctx.instance.slug = await this.slugify(ctx.instance['name']);
                console.log(ctx.instance.slug);
            }
            if (ctx && ctx.data) {
                ctx.data.updatedAt = moment().format('x');
            }
        });
        return modelClass;
    }
    async slugify(string) {
        return string.toString().trim().toLowerCase().replace(/\s+/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
    }
};
RoleRepository = tslib_1.__decorate([
    tslib_1.__param(0, core_1.inject('datasources.MySqlDataSource')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.MySqlDataSourceDataSource])
], RoleRepository);
exports.RoleRepository = RoleRepository;
//# sourceMappingURL=role.repository.js.map