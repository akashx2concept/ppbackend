import { DefaultCrudRepository } from '@loopback/repository';
import { MySqlDataSourceDataSource } from '../datasources';
import { Role, RoleRelations } from '../models';
export declare class RoleRepository extends DefaultCrudRepository<Role, typeof Role.prototype.id, RoleRelations> {
    constructor(dataSource: MySqlDataSourceDataSource);
    definePersistedModel(entityClass: typeof Role): typeof import("loopback-datasource-juggler").PersistedModel;
    slugify(string: string): Promise<string>;
}
