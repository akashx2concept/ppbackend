"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const models_1 = require("../models");
var moment = require('moment-timezone');
let UserRepository = class UserRepository extends repository_1.DefaultCrudRepository {
    constructor(
    // @inject('datasources.mongo') dataSource: MongoDataSource,
    dataSource, userCredentialsRepositoryGetter, roleRepositoryGetter) {
        super(models_1.User, dataSource);
        this.userCredentialsRepositoryGetter = userCredentialsRepositoryGetter;
        this.roleRepositoryGetter = roleRepositoryGetter;
        this.role = this.createBelongsToAccessorFor('role', roleRepositoryGetter);
        this.registerInclusionResolver('role', this.role.inclusionResolver);
        this.userCredentials = this.createHasOneRepositoryFactoryFor('userCredentials', userCredentialsRepositoryGetter);
    }
    definePersistedModel(entityClass) {
        const modelClass = super.definePersistedModel(entityClass);
        modelClass.observe('before save', async (ctx) => {
            // console.log(`going to save ${ctx.Model.modelName}`);
            if (ctx.instance && !ctx.instance.id) {
                ctx.instance.createdAt = moment().format('x');
            }
            if (ctx && ctx.data) {
                ctx.data.updatedAt = moment().format('x');
            }
        });
        return modelClass;
    }
    async findCredentials(userId) {
        try {
            return await this.userCredentials(userId).get();
        }
        catch (err) {
            if (err.code === 'ENTITY_NOT_FOUND') {
                return undefined;
            }
            throw err;
        }
    }
};
UserRepository = tslib_1.__decorate([
    tslib_1.__param(0, core_1.inject('datasources.MySqlDataSource')),
    tslib_1.__param(1, repository_1.repository.getter('UserCredentialsRepository')),
    tslib_1.__param(2, repository_1.repository.getter('RoleRepository')),
    tslib_1.__metadata("design:paramtypes", [repository_1.juggler.DataSource, Function, Function])
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map