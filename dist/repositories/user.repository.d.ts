import { Getter } from '@loopback/core';
import { DefaultCrudRepository, HasOneRepositoryFactory, juggler, BelongsToAccessor } from '@loopback/repository';
import { User, UserCredentials, UserRelations, Role } from '../models';
import { UserCredentialsRepository } from './user-credentials.repository';
import { RoleRepository } from './role.repository';
export declare class UserRepository extends DefaultCrudRepository<User, typeof User.prototype.id, UserRelations> {
    protected userCredentialsRepositoryGetter: Getter<UserCredentialsRepository>;
    protected roleRepositoryGetter: Getter<RoleRepository>;
    userCredentials: HasOneRepositoryFactory<UserCredentials, typeof User.prototype.id>;
    readonly role: BelongsToAccessor<Role, typeof User.prototype.id>;
    constructor(dataSource: juggler.DataSource, userCredentialsRepositoryGetter: Getter<UserCredentialsRepository>, roleRepositoryGetter: Getter<RoleRepository>);
    definePersistedModel(entityClass: typeof User): typeof juggler.PersistedModel;
    findCredentials(userId: typeof User.prototype.id): Promise<UserCredentials | undefined>;
}
export declare type Credentials = {
    email: string;
    password: string;
};
export declare type RefreshToken = {
    AccessToken: string;
    RefreshToken: string;
    userId: string;
};
export declare type ChangePassword = {
    oldPassword: string;
    newPassword: string;
    cnfNewPassword: string;
};
export declare type RefreshGrant = {
    refreshToken: string;
};
