"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImagesRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
var moment = require('moment-timezone');
let ImagesRepository = class ImagesRepository extends repository_1.DefaultCrudRepository {
    constructor(dataSource, userRepositoryGetter) {
        super(models_1.Images, dataSource);
        this.userRepositoryGetter = userRepositoryGetter;
        this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
        this.registerInclusionResolver('user', this.user.inclusionResolver);
    }
    definePersistedModel(entityClass) {
        const modelClass = super.definePersistedModel(entityClass);
        modelClass.observe('before save', async (ctx) => {
            // console.log(`going to save ${ctx.Model.modelName}`);
            if (ctx.instance && !ctx.instance.id) {
                ctx.instance.createdAt = moment().format('x');
            }
            if (ctx && ctx.data) {
                ctx.data.updatedAt = moment().format('x');
            }
        });
        return modelClass;
    }
};
ImagesRepository = tslib_1.__decorate([
    tslib_1.__param(0, core_1.inject('datasources.MySqlDataSource')),
    tslib_1.__param(1, repository_1.repository.getter('UserRepository')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.MySqlDataSourceDataSource, Function])
], ImagesRepository);
exports.ImagesRepository = ImagesRepository;
//# sourceMappingURL=images.repository.js.map