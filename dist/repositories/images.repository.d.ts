import { Getter } from '@loopback/core';
import { BelongsToAccessor, DefaultCrudRepository } from '@loopback/repository';
import { MySqlDataSourceDataSource } from '../datasources';
import { Images, ImagesRelations, User } from '../models';
import { UserRepository } from './user.repository';
export declare class ImagesRepository extends DefaultCrudRepository<Images, typeof Images.prototype.id, ImagesRelations> {
    protected userRepositoryGetter: Getter<UserRepository>;
    readonly user: BelongsToAccessor<User, typeof Images.prototype.id>;
    constructor(dataSource: MySqlDataSourceDataSource, userRepositoryGetter: Getter<UserRepository>);
    definePersistedModel(entityClass: typeof Images): typeof import("loopback-datasource-juggler").PersistedModel;
}
