"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScanningApplication = exports.PackageKey = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const authentication_jwt_1 = require("@loopback/authentication-jwt");
const authorization_1 = require("@loopback/authorization");
const boot_1 = require("@loopback/boot");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const rest_explorer_1 = require("@loopback/rest-explorer");
const service_proxy_1 = require("@loopback/service-proxy");
const keys_1 = require("./keys");
const middlewares_1 = require("./middlewares");
const repositories_1 = require("./repositories");
const sequence_1 = require("./sequence");
const services_1 = require("./services");
const globalConfig_service_1 = require("./services/globalConfig.service");
const YAML = require("yaml");
const fs_1 = tslib_1.__importDefault(require("fs"));
const path_1 = tslib_1.__importDefault(require("path"));
exports.PackageKey = core_1.BindingKey.create('application.package');
const pkg = require('../package.json');
class ScanningApplication extends boot_1.BootMixin(service_proxy_1.ServiceMixin(repository_1.RepositoryMixin(rest_1.RestApplication))) {
    constructor(options = {}) {
        super(options);
        // Bind authentication component related elements
        this.component(authentication_1.AuthenticationComponent);
        this.component(authentication_jwt_1.JWTAuthenticationComponent);
        this.component(authorization_1.AuthorizationComponent);
        this.setUpBindings();
        // Set up the custom sequence
        this.sequence(sequence_1.MySequence);
        // Set up default home page
        this.static('/', path_1.default.join(__dirname, '../public'));
        // Customize @loopback/rest-explorer configuration here
        this.configure(rest_explorer_1.RestExplorerBindings.COMPONENT).to({
            path: '/explorer',
        });
        this.component(rest_explorer_1.RestExplorerComponent);
        this.projectRoot = __dirname;
        // Customize @loopback/boot Booter Conventions here
        this.bootOptions = {
            controllers: {
                // Customize ControllerBooter Conventions here
                dirs: ['controllers'],
                extensions: ['.controller.js'],
                nested: true,
            },
        };
    }
    setUpBindings() {
        // Bind package.json to the application context
        this.bind(exports.PackageKey).to(pkg);
        // Bind bcrypt hash services
        this.bind(keys_1.PasswordHasherBindings.ROUNDS).to(10);
        this.bind(keys_1.PasswordHasherBindings.PASSWORD_HASHER).toClass(services_1.BcryptHasher);
        this.bind(authentication_jwt_1.TokenServiceBindings.TOKEN_SERVICE).toClass(services_1.JWTService);
        this.bind(keys_1.UserServiceBindings.USER_SERVICE).toClass(services_1.UserManagementService);
        this.add(core_1.createBindingFromClass(services_1.SecuritySpecEnhancer));
        this.add(core_1.createBindingFromClass(middlewares_1.ErrorHandlerMiddlewareProvider));
        // Use JWT secret from JWT_SECRET environment variable if set
        // otherwise create a random string of 64 hex digits
        // const secret = process.env.JWT_SECRET ?? crypto.randomBytes(32).toString('hex');
        this.bind(authentication_jwt_1.TokenServiceBindings.TOKEN_SECRET).to("aakash1147");
        // this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to("60");
        this.bind(authentication_jwt_1.RefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE).toClass(authentication_jwt_1.RefreshtokenService);
        this.bind(authentication_jwt_1.RefreshTokenServiceBindings.REFRESH_SECRET).to("aakash1147");
        this.bind(authentication_jwt_1.RefreshTokenServiceBindings.REFRESH_EXPIRES_IN).to("360000");
        this.bind(authentication_jwt_1.RefreshTokenServiceBindings.REFRESH_ISSUER).to("loopback4");
        //Global Config Service
        this.bind(keys_1.GlobalConfigBinding.Global_Config).toClass(globalConfig_service_1.GlobalConfigService);
        this.bind('authentication.jwt.secretKey').to('aakash1147');
        this.bind('authentication.jwt.expiresIn').to('60 days');
    }
    // Unfortunately, TypeScript does not allow overriding methods inherited
    // from mapped types. https://github.com/microsoft/TypeScript/issues/38496
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    async start() {
        // Use `databaseSeeding` flag to control if products/users should be pre
        // populated into the database. Its value is default to `true`.
        if (this.options.databaseSeeding !== false) {
            await this.migrateSchema();
        }
        return super.start();
    }
    async migrateSchema(options) {
        await super.migrateSchema(options);
        // Pre-populate users
        const roleRepository = await this.getRepository(repositories_1.RoleRepository);
        // await userRepo.deleteAll();
        const roleDir = path_1.default.join(__dirname, '../fixtures/role');
        const roleFiles = fs_1.default.readdirSync(roleDir);
        for (const file of roleFiles) {
            if (file.endsWith('.yml')) {
                const roleFile = path_1.default.join(roleDir, file);
                const yamlString = YAML.parse(fs_1.default.readFileSync(roleFile, 'utf8'));
                let roleData = await roleRepository.findOne({ where: { id: yamlString['id'] } });
                if (!roleData)
                    await roleRepository.create(yamlString);
            }
        }
    }
}
exports.ScanningApplication = ScanningApplication;
//# sourceMappingURL=application.js.map