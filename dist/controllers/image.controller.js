"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImageController = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let ImageController = class ImageController {
    constructor(imagesRepository) {
        this.imagesRepository = imagesRepository;
    }
    async create(images) {
        return this.imagesRepository.create(images);
    }
    async count(where) {
        return this.imagesRepository.count(where);
    }
    async find(filter) {
        return this.imagesRepository.find(filter);
    }
};
tslib_1.__decorate([
    rest_1.post('/api/images'),
    rest_1.response(200, {
        description: 'Images model instance',
        content: { 'application/json': { schema: rest_1.getModelSchemaRef(models_1.Images) } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Images, {
                    title: 'NewImages',
                    exclude: ['id'],
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ImageController.prototype, "create", null);
tslib_1.__decorate([
    rest_1.get('/api/images/count'),
    rest_1.response(200, {
        description: 'Images model count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.param.where(models_1.Images)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ImageController.prototype, "count", null);
tslib_1.__decorate([
    rest_1.get('/api/images'),
    rest_1.response(200, {
        description: 'Array of Images model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: rest_1.getModelSchemaRef(models_1.Images, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Images)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ImageController.prototype, "find", null);
ImageController = tslib_1.__decorate([
    tslib_1.__param(0, repository_1.repository(repositories_1.ImagesRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.ImagesRepository])
], ImageController);
exports.ImageController = ImageController;
//# sourceMappingURL=image.controller.js.map