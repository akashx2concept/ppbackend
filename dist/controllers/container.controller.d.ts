/// <reference types="express" />
import { Request, Response } from '@loopback/rest';
import { GlobalConfigService } from '../services';
import { ImagesRepository } from '../repositories';
export declare class StorageGcController {
    imagesRepository: ImagesRepository;
    request: Request;
    response: Response;
    globalConfigService: GlobalConfigService;
    private storageGcSvc;
    constructor(imagesRepository: ImagesRepository, request: Request, response: Response, globalConfigService: GlobalConfigService);
    deleteFileInContainer(containerName: string, fileName: string): Promise<boolean>;
    upload(containerName: string): Promise<any>;
    download(containerName: string, fileName: string): Promise<any>;
}
