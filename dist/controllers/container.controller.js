"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StorageGcController = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const authentication_1 = require("@loopback/authentication");
const rest_1 = require("@loopback/rest");
const repository_1 = require("@loopback/repository");
const util_1 = require("util");
const models_1 = require("../models");
const keys_1 = require("../keys");
const services_1 = require("../services");
const repositories_1 = require("../repositories");
let StorageGcController = class StorageGcController {
    constructor(imagesRepository, request, response, globalConfigService) {
        this.imagesRepository = imagesRepository;
        this.request = request;
        this.response = response;
        this.globalConfigService = globalConfigService;
    }
    //   @post('/containers', {
    //     responses: {
    //       '200': {
    //         description: 'Container model instance',
    //         content: { 'application/json': { schema: { 'x-ts-type': Container } } },
    //       },
    //     },
    //   })
    //   async createContainer(@requestBody() container: Container): Promise<Container> {
    //     const createContainer = promisify(this.storageGcSvc.createContainer);
    //     return await createContainer(container);
    //   }
    //   @get('/containers', {
    //     responses: {
    //       '200': {
    //         description: 'Array of Containers model instances',
    //         content: {
    //           'application/json': {
    //             schema: { type: 'array', items: { 'x-ts-type': Container } },
    //           },
    //         },
    //       },
    //     },
    //   })
    //   async findContainer(@param.query.object('filter', getFilterSchemaFor(Container)) filter?: Filter): Promise<Container[]> {
    //     const getContainers = promisify(this.storageGcSvc.getContainers);
    //     return await getContainers();
    //   }
    //   @get('/containers/{containerName}', {
    //     responses: {
    //       '200': {
    //         description: 'Container model instance',
    //         content: { 'application/json': { schema: { 'x-ts-type': Container } } },
    //       },
    //     },
    //   })
    //   async findContainerByName(@param.path.string('containerName') containerName: string): Promise<Container> {
    //     const getContainer = promisify(this.storageGcSvc.getContainer);
    //     return await getContainer(containerName);
    //   }
    //   @del('/containers/{containerName}', {
    //     responses: {
    //       '204': {
    //         description: 'Container DELETE success',
    //       },
    //     },
    //   })
    //   async deleteContainerByName(@param.path.string('containerName') containerName: string): Promise<boolean> {
    //     const destroyContainer = promisify(this.storageGcSvc.destroyContainer);
    //     return await destroyContainer(containerName);
    //   }
    //   @get('/containers/{containerName}/files', {
    //     responses: {
    //       '200': {
    //         description: 'Array of Files model instances belongs to container',
    //         content: {
    //           'application/json': {
    //             schema: { type: 'array', items: { 'x-ts-type': File } },
    //           },
    //         },
    //       },
    //     },
    //   })
    //   async findFilesInContainer(@param.path.string('containerName') containerName: string,
    //     @param.query.object('filter', getFilterSchemaFor(Container)) filter?: Filter): Promise<File[]> {
    //     const getFiles = promisify(this.storageGcSvc.getFiles);
    //     return await getFiles(containerName, {});
    //   }
    // @get('/containers/{containerName}/files/{fileName}', {
    //   responses: {
    //     '200': {
    //       description: 'File model instances belongs to container',
    //       content: { 'application/json': { schema: { 'x-ts-type': File } } },
    //     },
    //   },
    // })
    // @authenticate('jwt')
    // @authorize({
    //   allowedRoles: ['Root', 'admin', 'User'],
    //   voters: [basicAuthorization],
    // })
    // async findFileInContainer(@param.path.string('containerName') containerName: string,
    //   @param.path.string('fileName') fileName: string): Promise<File> {
    //   const getFile = promisify(this.storageGcSvc.getFile);
    //   return await getFile(this.globalConfigService.S3Container, fileName);
    // }
    // @authorize({
    //   allowedRoles: ['Root', 'admin', 'User'],
    //   voters: [basicAuthorization],
    // })
    async deleteFileInContainer(containerName, fileName) {
        const removeFile = util_1.promisify(this.storageGcSvc.removeFile);
        return await removeFile(this.globalConfigService.S3Container, fileName);
    }
    // @authorize({
    //   allowedRoles: ['Root', 'admin', 'User'],
    //   voters: [basicAuthorization],
    // })
    async upload(containerName) {
        const upload = util_1.promisify(this.storageGcSvc.upload);
        let uploadDoc = await upload(this.globalConfigService.S3Container, this.request, this.response, {});
        let CONTAINER_URL = this.globalConfigService.S3Container;
        var fileInfoArr = uploadDoc.files.file;
        // var objs = {};
        // fileInfoArr.forEach(function(item:any) {
        //     objs = {
        //       originalName: item.name,
        //       displayName: item.name,
        //       type: item.type,
        //       url: `containers/${CONTAINER_URL}/download/${item.name}`,
        //     };
        // });    
        // return await this.imagesRepository.create(objs);
        return { url: `api/containers/${CONTAINER_URL}/download/${fileInfoArr[0].name}` };
    }
    // @authenticate('jwt')
    // @authorize({
    //   allowedRoles: ['Root', 'admin', 'User'],
    //   voters: [basicAuthorization],
    // })
    async download(containerName, fileName) {
        const download = util_1.promisify(this.storageGcSvc.download);
        return await download(this.globalConfigService.S3Container, fileName, this.request, this.response);
    }
};
tslib_1.__decorate([
    core_1.inject('services.StorageGCService'),
    tslib_1.__metadata("design:type", Object)
], StorageGcController.prototype, "storageGcSvc", void 0);
tslib_1.__decorate([
    rest_1.del('/api/containers/{containerName}/files/{fileName}', {
        responses: {
            '204': {
                description: 'File DELETE from Container success',
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    tslib_1.__param(0, rest_1.param.path.string('containerName')),
    tslib_1.__param(1, rest_1.param.path.string('fileName')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], StorageGcController.prototype, "deleteFileInContainer", null);
tslib_1.__decorate([
    rest_1.post('/api/containers/{containerName}/upload', {
        responses: {
            '200': {
                description: 'Upload a Files model instances into Container',
                content: { 'application/json': { schema: { 'x-ts-type': models_1.Images } } },
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    tslib_1.__param(0, rest_1.param.path.string('containerName')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], StorageGcController.prototype, "upload", null);
tslib_1.__decorate([
    rest_1.get(`/api/containers/{containerName}/download/{fileName}`, {
        responses: {
            '200': {
                description: 'Download a File within specified Container',
                content: { 'application/json': { schema: { 'x-ts-type': Object } } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('containerName')),
    tslib_1.__param(1, rest_1.param.path.string('fileName')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], StorageGcController.prototype, "download", null);
StorageGcController = tslib_1.__decorate([
    tslib_1.__param(0, repository_1.repository(repositories_1.ImagesRepository)),
    tslib_1.__param(1, core_1.inject(rest_1.RestBindings.Http.REQUEST)),
    tslib_1.__param(2, core_1.inject(rest_1.RestBindings.Http.RESPONSE)),
    tslib_1.__param(3, core_1.inject(keys_1.GlobalConfigBinding.Global_Config)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.ImagesRepository, Object, Object, services_1.GlobalConfigService])
], StorageGcController);
exports.StorageGcController = StorageGcController;
//# sourceMappingURL=container.controller.js.map