import { TokenService, UserService } from '@loopback/authentication';
import { RefreshTokenService, TokenObject } from '@loopback/authentication-jwt';
import { Count, Filter, Where } from '@loopback/repository';
import { UserProfile } from '@loopback/security';
import { KeyAndPassword, ResetPasswordInit, User } from '../models';
import { ChangePassword, Credentials, UserRepository, RefreshGrant, RoleRepository, ImagesRepository } from '../repositories';
import { GlobalConfigService, PasswordHasher, UserManagementService } from '../services';
export declare class NewUserRequest extends User {
    password: string;
}
export declare class UserController {
    userRepository: UserRepository;
    roleRepository: RoleRepository;
    imageRepository: ImagesRepository;
    passwordHasher: PasswordHasher;
    jwtService: TokenService;
    userService: UserService<User, Credentials>;
    userManagementService: UserManagementService;
    refreshService: RefreshTokenService;
    globalConfigService: GlobalConfigService;
    constructor(userRepository: UserRepository, roleRepository: RoleRepository, imageRepository: ImagesRepository, passwordHasher: PasswordHasher, jwtService: TokenService, userService: UserService<User, Credentials>, userManagementService: UserManagementService, refreshService: RefreshTokenService, globalConfigService: GlobalConfigService);
    find(filter?: Filter<User>): Promise<User[]>;
    count(where?: Where<User>): Promise<Count>;
    signUp(newUserRequest: NewUserRequest): Promise<User>;
    set(currentUserProfile: UserProfile, userId: any, user: User): Promise<void>;
    findById(userId: any): Promise<User>;
    printCurrentUser(currentUserProfile: UserProfile): Promise<any>;
    login(credentials: Credentials): Promise<{
        token: any;
    }>;
    forgotPassword(currentUserProfile: UserProfile, credentials: Credentials): Promise<{
        token: string;
    }>;
    resetPasswordInit(resetPasswordInit: ResetPasswordInit): Promise<any>;
    resetPasswordFinish(keyAndPassword: KeyAndPassword): Promise<string>;
    chnagePassword(currentUserProfile: UserProfile, changePassword: ChangePassword): Promise<ChangePassword>;
    refreshLogin(credentials: Credentials): Promise<TokenObject>;
    refresh(refreshGrant: RefreshGrant): Promise<TokenObject>;
    createUser(newUserRequest: NewUserRequest): Promise<User>;
    dashboardAnalysis(userId: any): Promise<any>;
}
