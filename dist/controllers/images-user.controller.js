"use strict";
// import {
//   repository,
// } from '@loopback/repository';
// import {
//   param,
//   get,
//   getModelSchemaRef,
// } from '@loopback/rest';
// import {
//   Images,
//   User,
// } from '../models';
// import {ImagesRepository} from '../repositories';
// export class ImagesUserController {
//   constructor(
//     @repository(ImagesRepository)
//     public imagesRepository: ImagesRepository,
//   ) { }
//   @get('/images/{id}/user', {
//     responses: {
//       '200': {
//         description: 'User belonging to Images',
//         content: {
//           'application/json': {
//             schema: {type: 'array', items: getModelSchemaRef(User)},
//           },
//         },
//       },
//     },
//   })
//   async getUser(
//     @param.path.string('id') id: typeof Images.prototype.id,
//   ): Promise<User> {
//     return this.imagesRepository.user(id);
//   }
// }
//# sourceMappingURL=images-user.controller.js.map