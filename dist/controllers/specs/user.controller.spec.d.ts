import { Model } from '@loopback/repository';
export declare class LoginSchema extends Model {
    email: string;
    password: string;
}
export declare const LoginRequestBody: {
    description: string;
    required: boolean;
    content: {
        'application/json': {
            schema: {
                'x-ts-type': typeof LoginSchema;
            };
        };
    };
};
