"use strict";
// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefreshGrantRequestBody = exports.ChangePasswordSchemaRequestBody = exports.RefreshTokenRequestBody = exports.PasswordResetRequestBody = exports.CredentialsRequestBody = exports.UserProfileSchema = void 0;
// TODO(jannyHou): This should be moved to @loopback/authentication
exports.UserProfileSchema = {
    type: 'object',
    required: ['id'],
    properties: {
        id: { type: 'string' },
        email: { type: 'string' },
        name: { type: 'string' },
        roleId: { type: 'number' },
    },
};
// TODO(jannyHou): This is a workaround to manually
// describe the request body of 'Users/login'.
// We should either create a Credential model, or
// infer the spec from User model
const CredentialsSchema = {
    type: 'object',
    required: ['email', 'password'],
    properties: {
        email: {
            type: 'string',
            format: 'email',
        },
        password: {
            type: 'string',
            minLength: 8,
        },
    },
};
exports.CredentialsRequestBody = {
    description: 'The input of login function',
    required: true,
    content: {
        'application/json': { schema: CredentialsSchema },
    },
};
exports.PasswordResetRequestBody = {
    description: 'The input of password reset function',
    required: true,
    content: {
        'application/json': { schema: CredentialsSchema },
    },
};
const RefreshTokenSchema = {
    type: 'object',
    required: ['AccessToken', 'RefreshToken', 'userId'],
    properties: {
        AccessToken: {
            type: 'string',
        },
        RefreshToken: {
            type: 'string',
        },
        userId: {
            type: 'string'
        }
    },
};
exports.RefreshTokenRequestBody = {
    description: 'The input of Refresh Token Schema',
    required: true,
    content: {
        'application/json': { schema: RefreshTokenSchema },
    },
};
const ChangePasswordSchema = {
    type: 'object',
    required: ['oldPassword', 'newPassword', 'cnfNewPassword'],
    properties: {
        oldPassword: {
            type: 'string',
            minLength: 8,
        },
        newPassword: {
            type: 'string',
            minLength: 8,
        },
        cnfNewPassword: {
            type: 'string',
            minLength: 8,
        },
    },
};
exports.ChangePasswordSchemaRequestBody = {
    description: 'The input of Refresh Token Schema',
    required: true,
    content: {
        'application/json': { schema: ChangePasswordSchema },
    },
};
// Describes the schema of grant object
const RefreshGrantSchema = {
    type: 'object',
    required: ['refreshToken'],
    properties: {
        refreshToken: {
            type: 'string',
        },
    },
};
// Describes the request body of grant object
exports.RefreshGrantRequestBody = {
    description: 'Reissuing Acess Token',
    required: true,
    content: {
        'application/json': { schema: RefreshGrantSchema },
    },
};
//# sourceMappingURL=user-controller.specs.js.map