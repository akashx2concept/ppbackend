"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginRequestBody = exports.LoginSchema = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
let LoginSchema = class LoginSchema extends repository_1.Model {
};
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], LoginSchema.prototype, "email", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], LoginSchema.prototype, "password", void 0);
LoginSchema = tslib_1.__decorate([
    repository_1.model()
], LoginSchema);
exports.LoginSchema = LoginSchema;
exports.LoginRequestBody = {
    description: 'The input of login function',
    required: true,
    content: { 'application/json': { schema: { 'x-ts-type': LoginSchema } } },
};
//# sourceMappingURL=user.controller.spec.js.map