import { Count, Filter, Where } from '@loopback/repository';
import { Images } from '../models';
import { ImagesRepository } from '../repositories';
export declare class ImageController {
    imagesRepository: ImagesRepository;
    constructor(imagesRepository: ImagesRepository);
    create(images: Omit<Images, 'id'>): Promise<Images>;
    count(where?: Where<Images>): Promise<Count>;
    find(filter?: Filter<Images>): Promise<Images[]>;
}
