"use strict";
// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = exports.NewUserRequest = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const authentication_jwt_1 = require("@loopback/authentication-jwt");
const authorization_1 = require("@loopback/authorization");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const repository_2 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const security_1 = require("@loopback/security");
const isemail_1 = tslib_1.__importDefault(require("isemail"));
const lodash_1 = tslib_1.__importDefault(require("lodash"));
const keys_1 = require("../keys");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
const services_1 = require("../services");
const utils_1 = require("../utils");
const user_controller_specs_1 = require("./specs/user-controller.specs");
var moment = require('moment-timezone');
let NewUserRequest = class NewUserRequest extends models_1.User {
};
tslib_1.__decorate([
    repository_2.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], NewUserRequest.prototype, "password", void 0);
NewUserRequest = tslib_1.__decorate([
    repository_2.model()
], NewUserRequest);
exports.NewUserRequest = NewUserRequest;
let UserController = class UserController {
    constructor(userRepository, roleRepository, imageRepository, passwordHasher, jwtService, userService, userManagementService, refreshService, globalConfigService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.imageRepository = imageRepository;
        this.passwordHasher = passwordHasher;
        this.jwtService = jwtService;
        this.userService = userService;
        this.userManagementService = userManagementService;
        this.refreshService = refreshService;
        this.globalConfigService = globalConfigService;
    }
    async find(filter) {
        return this.userRepository.find(filter);
    }
    async count(where) {
        return this.userRepository.count(where);
    }
    async signUp(newUserRequest) {
        // All new users have the "customer" role by default
        let roleData = await this.roleRepository.findOne({ where: { slug: "super-admin" } });
        if (roleData) {
            newUserRequest.roleId = roleData['id'];
            // ensure a valid email value and password value
            services_1.validateCredentials(lodash_1.default.pick(newUserRequest, ['email', 'password']));
            try {
                newUserRequest.resetKey = '';
                return await this.userManagementService.createUser(newUserRequest);
            }
            catch (error) {
                console.log(error);
                // MongoError 11000 duplicate key
                if (error.code === 11000 && error.errmsg.includes('index: uniqueEmail')) {
                    throw new rest_1.HttpErrors.Conflict('Email value is already taken');
                }
                else {
                    throw error;
                }
            }
        }
        else {
            throw new rest_1.HttpErrors.NotFound('Role not found');
        }
    }
    // @authorize({
    //   allowedRoles: ['Root', 'admin', 'User'],
    //   voters: [basicAuthorization],
    // })
    async set(currentUserProfile, userId, 
    // @requestBody({description: 'update user'}) user: User,
    user) {
        try {
            // Only admin can assign roles
            if (currentUserProfile.roleId) {
                delete user.roleId;
            }
            return await this.userRepository.updateById(userId, user);
        }
        catch (e) {
            return e;
        }
    }
    async findById(userId) {
        return this.userRepository.findById(userId);
    }
    async printCurrentUser(currentUserProfile) {
        // (@jannyHou)FIXME: explore a way to generate OpenAPI schema
        // for symbol property
        const userId = currentUserProfile[security_1.securityId];
        return await this.userRepository.findOne({ where: { id: userId }, include: [{ relation: "role" }] });
    }
    async login(credentials) {
        // ensure the user exists, and the password is correct
        const user = await this.userService.verifyCredentials(credentials);
        // console.log(user);
        // convert a User object into a UserProfile object (reduced set of properties)
        const userProfile = this.userService.convertToUserProfile(user);
        // create a JSON Web Token based on the user profile
        let token = await this.jwtService.generateToken(userProfile);
        return { token };
    }
    async forgotPassword(currentUserProfile, credentials) {
        const { email, password } = credentials;
        const { id } = currentUserProfile;
        const user = await this.userRepository.findById(id);
        if (!user) {
            throw new rest_1.HttpErrors.NotFound('User account not found');
        }
        if (email !== (user === null || user === void 0 ? void 0 : user.email)) {
            throw new rest_1.HttpErrors.Forbidden('Invalid email address');
        }
        services_1.validateCredentials(lodash_1.default.pick(credentials, ['email', 'password']));
        const passwordHash = await this.passwordHasher.hashPassword(password);
        await this.userRepository.userCredentials(user.id).patch({ password: passwordHash });
        const userProfile = this.userService.convertToUserProfile(user);
        const token = await this.jwtService.generateToken(userProfile);
        return { token };
    }
    async resetPasswordInit(resetPasswordInit) {
        if (!isemail_1.default.validate(resetPasswordInit.email)) {
            throw new rest_1.HttpErrors.UnprocessableEntity('Invalid email address');
        }
        const nodeMailer = await this.userManagementService.requestPasswordReset(resetPasswordInit.email);
        if (nodeMailer['massage'] == 'success') {
            return { massage: 'Successfully sent reset password link' };
        }
        throw new rest_1.HttpErrors.InternalServerError('Error sending reset password email');
    }
    async resetPasswordFinish(keyAndPassword) {
        services_1.validateKeyPassword(keyAndPassword);
        const foundUser = await this.userRepository.findOne({
            where: { resetKey: keyAndPassword.resetKey },
        });
        if (!foundUser) {
            throw new rest_1.HttpErrors.NotFound('No associated account for the provided reset key');
        }
        const user = await this.userManagementService.validateResetKeyLifeSpan(foundUser);
        const passwordHash = await this.passwordHasher.hashPassword(keyAndPassword.password);
        try {
            await this.userRepository
                .userCredentials(user.id)
                .patch({ password: passwordHash });
            await this.userRepository.updateById(user.id, user);
        }
        catch (e) {
            return e;
        }
        return 'Password reset successful';
    }
    // @post('/refreshToken', {
    //   responses: {
    //     '200': {
    //       description: 'Token',
    //       content: {
    //         'application/json': {
    //           schema: {
    //             type: 'object',
    //             properties: {
    //               token: {
    //                 type: 'string',
    //               },
    //             },
    //           },
    //         },
    //       },
    //     },
    //   },
    // })
    // async refreshToken(@requestBody(RefreshTokenRequestBody) refreshToken: RefreshToken,): Promise<RefreshToken> {
    //   try {
    //     let tokenData:any = await this.accessTokenRepository.findOne({where: {  AccessToken: refreshToken['AccessToken'], RefreshToken: refreshToken['RefreshToken'], userId: refreshToken['userId'] }});
    //     if (tokenData) {
    //       const RefreshToken = refreshToken['RefreshToken']
    //       const RefreshToken_Verified = await this.userManagementService.verifyToken(RefreshToken);        
    //       const user = await this.userRepository.findById(refreshToken['userId']);
    //       const userProfile = this.userService.convertToUserProfile(user);
    //       let token = await this.jwtService.generateToken(userProfile);
    //       let refreshToekn = await this.userManagementService.generateRefreshToken(userProfile);
    //       let data:any = {
    //         id: tokenData['id'],
    //         AccessToken: token,
    //         RefreshToken: refreshToekn,
    //         userId: userProfile['id']
    //       }
    //       await this.accessTokenRepository.replaceById(data.id, data);
    //       const newToken:any = await this.accessTokenRepository.findById(data['id']);
    //       return newToken;
    //     } else {
    //       throw new HttpErrors.NotFound('Refresh Token not found',);
    //     }
    //   } catch (error) { 
    //     return error;
    //   }
    // }
    async chnagePassword(currentUserProfile, changePassword) {
        try {
            console.log(currentUserProfile);
            return changePassword;
        }
        catch (error) {
            return error;
        }
    }
    async refreshLogin(credentials) {
        // ensure the user exists, and the password is correct
        const user = await this.userService.verifyCredentials(credentials);
        // console.log(user);
        // convert a User object into a UserProfile object (reduced set of properties)
        const userProfile = this.userService.convertToUserProfile(user);
        const accessToken = await this.jwtService.generateToken(userProfile);
        const tokens = await this.refreshService.generateToken(userProfile, accessToken);
        return tokens;
    }
    async refresh(refreshGrant) {
        return this.refreshService.refreshToken(refreshGrant.refreshToken);
    }
    async createUser(newUserRequest) {
        // All new users have the "customer" role by default
        let roleData = await this.roleRepository.findOne({ where: { slug: "user" } });
        if (roleData) {
            newUserRequest.roleId = roleData['id'];
            // ensure a valid email value and password value
            services_1.validateCredentials(lodash_1.default.pick(newUserRequest, ['email', 'password']));
            try {
                newUserRequest.resetKey = '';
                let createdUser = await this.userManagementService.createUser(newUserRequest);
                let imageDataDto = [{
                        url: `api/containers/${this.globalConfigService.S3Container}/download/folderImage.svg`,
                        isFolder: true,
                        displayName: "Bills",
                        originalName: "Bills",
                        createdBy: createdUser['id'],
                        userId: createdUser['id']
                    }, {
                        url: `api/containers/${this.globalConfigService.S3Container}/download/folderImage.svg`,
                        isFolder: true,
                        displayName: "Recipts",
                        originalName: "Recipts",
                        createdBy: createdUser['id'],
                        userId: createdUser['id']
                    }];
                await this.imageRepository.createAll(imageDataDto);
                return createdUser;
            }
            catch (error) {
                console.log(error);
                // MongoError 11000 duplicate key
                if (error.code === 11000 && error.errmsg.includes('index: uniqueEmail')) {
                    throw new rest_1.HttpErrors.Conflict('Email value is already taken');
                }
                else {
                    throw error;
                }
            }
        }
        else {
            throw new rest_1.HttpErrors.NotFound('Role not found');
        }
    }
    // @authorize({
    //   allowedRoles: ['Root', 'admin', 'User'],
    //   voters: [basicAuthorization],
    // })
    async dashboardAnalysis(userId) {
        var lineActiveLabels = [];
        var lineInActiveData = [];
        var lineChartLabels = [];
        for (let i = 11; i >= 0; i--) {
            if (i == 0) {
                lineChartLabels.push(moment().format("MMM YYYY"));
                var month_dayStart = moment().startOf('month').format('x');
                var month_dayEnd = moment().endOf('day').format("x");
                let isActiveQuery = { isActive: true, createdBy: userId, createdAt: { between: [month_dayStart, month_dayEnd] } };
                var isActiveCount = await this.userRepository.count(isActiveQuery);
                lineActiveLabels.push(isActiveCount.count);
                let isInActiveQuery = { isActive: false, createdBy: userId, createdAt: { between: [month_dayStart, month_dayEnd] } };
                var isActiveCount = await this.userRepository.count(isInActiveQuery);
                lineInActiveData.push(isActiveCount.count);
            }
            else {
                lineChartLabels.push(moment().subtract(i, 'month').format("MMM YYYY"));
                var month_dayStart = moment(moment().subtract(i, 'month')).startOf('month').format("x");
                var month_dayEnd = moment(moment().subtract(i, 'month')).endOf('month').format("x");
                let isActiveQuery = { isActive: true, createdBy: userId, createdAt: { between: [month_dayStart, month_dayEnd] } };
                var isActiveCount = await this.userRepository.count(isActiveQuery);
                lineActiveLabels.push(isActiveCount.count);
                let isInActiveQuery = { isActive: false, createdBy: userId, createdAt: { between: [month_dayStart, month_dayEnd] } };
                var isActiveCount = await this.userRepository.count(isInActiveQuery);
                lineInActiveData.push(isActiveCount.count);
            }
        }
        let graphData = {
            lineActiveLabels: lineActiveLabels,
            lineInActiveData: lineInActiveData,
            lineChartLabels: lineChartLabels
        };
        return graphData;
        // return this.userRepository.findById(userId);
    }
};
tslib_1.__decorate([
    rest_1.get('/api/users'),
    rest_1.response(200, {
        description: 'Array of User model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: rest_1.getModelSchemaRef(models_1.User, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.User)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "find", null);
tslib_1.__decorate([
    rest_1.get('/api/users/count'),
    rest_1.response(200, {
        description: 'Role model count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.param.where(models_1.User)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "count", null);
tslib_1.__decorate([
    rest_1.post('/api/signUp', {
        responses: {
            '200': {
                description: 'User',
                content: {
                    'application/json': {
                        schema: {
                            'x-ts-type': models_1.User,
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [NewUserRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "signUp", null);
tslib_1.__decorate([
    rest_1.put('/api/users/{userId}', {
        security: utils_1.OPERATION_SECURITY_SPEC,
        responses: {
            '200': {
                description: 'User',
                content: {
                    'application/json': {
                        schema: {
                            'x-ts-type': models_1.User,
                        },
                    },
                },
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    tslib_1.__param(0, core_1.inject(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, rest_1.param.path.string('userId')),
    tslib_1.__param(2, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.User, { partial: true }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, models_1.User]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "set", null);
tslib_1.__decorate([
    rest_1.get('/api/users/{userId}', {
        security: utils_1.OPERATION_SECURITY_SPEC,
        responses: {
            '200': {
                description: 'User',
                content: {
                    'application/json': {
                        schema: {
                            'x-ts-type': models_1.User,
                        },
                    },
                },
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    authorization_1.authorize({
        allowedRoles: ['Root', 'admin', 'User'],
        voters: [services_1.basicAuthorization],
    }),
    tslib_1.__param(0, rest_1.param.path.string('userId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "findById", null);
tslib_1.__decorate([
    rest_1.get('/api/users/me', {
        security: utils_1.OPERATION_SECURITY_SPEC,
        responses: {
            '200': {
                description: 'The current user profile',
                content: {
                    'application/json': {
                        schema: user_controller_specs_1.UserProfileSchema,
                    },
                },
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    tslib_1.__param(0, core_1.inject(security_1.SecurityBindings.USER)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "printCurrentUser", null);
tslib_1.__decorate([
    rest_1.post('/api/users/login', {
        responses: {
            '200': {
                description: 'Token',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                token: {
                                    type: 'string',
                                },
                            },
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody(user_controller_specs_1.CredentialsRequestBody)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "login", null);
tslib_1.__decorate([
    rest_1.put('/api/users/forgot-password', {
        security: utils_1.OPERATION_SECURITY_SPEC,
        responses: {
            '200': {
                description: 'The updated user profile',
                content: {
                    'application/json': {
                        schema: user_controller_specs_1.UserProfileSchema,
                    },
                },
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    tslib_1.__param(0, core_1.inject(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, rest_1.requestBody(user_controller_specs_1.PasswordResetRequestBody)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "forgotPassword", null);
tslib_1.__decorate([
    rest_1.post('/api/users/reset-password/init', {
        responses: {
            '200': {
                description: 'Confirmation that reset password email has been sent',
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.ResetPasswordInit]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "resetPasswordInit", null);
tslib_1.__decorate([
    rest_1.put('/api/users/reset-password/finish', {
        responses: {
            '200': {
                description: 'A successful password reset response',
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.KeyAndPassword]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "resetPasswordFinish", null);
tslib_1.__decorate([
    rest_1.post('/api/change-password', {
        responses: {
            '200': {
                description: 'Token',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                token: {
                                    type: 'string',
                                },
                            },
                        },
                    },
                },
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    tslib_1.__param(0, core_1.inject(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, rest_1.requestBody(user_controller_specs_1.ChangePasswordSchemaRequestBody)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "chnagePassword", null);
tslib_1.__decorate([
    rest_1.post('/api/users/refresh-login', {
        responses: {
            '200': {
                description: 'Token',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                accessToken: {
                                    type: 'string',
                                },
                                refreshToken: {
                                    type: 'string',
                                },
                            },
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody(user_controller_specs_1.CredentialsRequestBody)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "refreshLogin", null);
tslib_1.__decorate([
    rest_1.post('/api/refresh', {
        responses: {
            '200': {
                description: 'Token',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                accessToken: {
                                    type: 'object',
                                },
                            },
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody(user_controller_specs_1.RefreshGrantRequestBody)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "refresh", null);
tslib_1.__decorate([
    rest_1.post('/api/createUser', {
        responses: {
            '200': {
                description: 'User',
                content: {
                    'application/json': {
                        schema: {
                            'x-ts-type': models_1.User,
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: { 'application/json': {
                schema: rest_1.getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [NewUserRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "createUser", null);
tslib_1.__decorate([
    rest_1.get('/api/users/dashboardAnalysis/{userId}', {
        security: utils_1.OPERATION_SECURITY_SPEC,
        responses: {
            '200': {
                description: 'User',
                content: {
                    'application/json': {
                        schema: {
                            'x-ts-type': models_1.User,
                        },
                    },
                },
            },
        },
    }),
    authentication_1.authenticate('jwt'),
    tslib_1.__param(0, rest_1.param.path.string('userId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "dashboardAnalysis", null);
UserController = tslib_1.__decorate([
    tslib_1.__param(0, repository_2.repository(repositories_1.UserRepository)),
    tslib_1.__param(1, repository_2.repository(repositories_1.RoleRepository)),
    tslib_1.__param(2, repository_2.repository(repositories_1.ImagesRepository)),
    tslib_1.__param(3, core_1.inject(keys_1.PasswordHasherBindings.PASSWORD_HASHER)),
    tslib_1.__param(4, core_1.inject(authentication_jwt_1.TokenServiceBindings.TOKEN_SERVICE)),
    tslib_1.__param(5, core_1.inject(keys_1.UserServiceBindings.USER_SERVICE)),
    tslib_1.__param(6, core_1.inject(keys_1.UserServiceBindings.USER_SERVICE)),
    tslib_1.__param(7, core_1.inject(authentication_jwt_1.RefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE)),
    tslib_1.__param(8, core_1.inject(keys_1.GlobalConfigBinding.Global_Config)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.UserRepository,
        repositories_1.RoleRepository,
        repositories_1.ImagesRepository, Object, Object, Object, services_1.UserManagementService, Object, services_1.GlobalConfigService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map