import {MiddlewareSequence} from '@loopback/rest';
import {inject} from '@loopback/context';
import {
  FindRoute,
  InvokeMethod,
  ParseParams,
  Reject,
  RequestContext,
  RestBindings,
  Send,
  SequenceHandler,
  InvokeMiddleware
} from '@loopback/rest';
import {
  AuthenticationBindings,
  AuthenticateFn,
  AUTHENTICATION_STRATEGY_NOT_FOUND,
  USER_PROFILE_NOT_FOUND,
} from '@loopback/authentication';


const SequenceActions = RestBindings.SequenceActions;

// extends MiddlewareSequence
export class MySequence implements SequenceHandler {

    /**
   * Optional invoker for registered middleware in a chain.
   * To be injected via SequenceActions.INVOKE_MIDDLEWARE.
   */
     @inject(SequenceActions.INVOKE_MIDDLEWARE, {optional: true})
     protected invokeMiddleware: InvokeMiddleware = () => false;

    constructor(
        @inject(SequenceActions.FIND_ROUTE) protected findRoute: FindRoute,
        @inject(SequenceActions.PARSE_PARAMS) protected parseParams: ParseParams,
        @inject(SequenceActions.INVOKE_METHOD) protected invoke: InvokeMethod,
        @inject(SequenceActions.SEND) public send: Send,
        @inject(SequenceActions.REJECT) public reject: Reject,
        @inject(AuthenticationBindings.AUTH_ACTION) protected authenticateRequest: AuthenticateFn,
    ) {}


    async handle(context: RequestContext) {
      try {
        // const {request, response} = context;
        // const route = this.findRoute(request);
        // //call authentication action
        // await this.authenticateRequest(request);
        // const args = await this.parseParams(request, route);
        // const result = await this.invoke(route, args);
        // this.send(response, result);

        const {request, response} = context;
        const finished = await this.invokeMiddleware(context);
        if (finished) return;
        const route = this.findRoute(request);
        await this.authenticateRequest(request);
        const args = await this.parseParams(request, route);
        const result = await this.invoke(route, args);
        this.send(response, result);

      } catch (error) {
        if (
          error.code === AUTHENTICATION_STRATEGY_NOT_FOUND ||
          error.code === USER_PROFILE_NOT_FOUND
        ) {
          Object.assign(error, {statusCode: 401 /* Unauthorized */});
        } else {
          Object.assign(error, {statusCode: 400});
        }          
        this.reject(context, error);
      }
    }

}
