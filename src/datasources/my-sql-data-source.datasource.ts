import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

// const config = {
//   name: 'MySqlDataSource',
//   connector: 'mysql',
//   url: '',
//   host: 'localhost',
//   port: 3306,
//   user: 'root',
//   password: 'Admin@123',
//   database: 'ppScanningQA'
// };

const config = {
  name: 'MySqlDataSource',
  connector: 'mysql',
  url: '',
  host: 'ls-2529ec5900ad5fc2cb01e06f551e79d7b40814ef.ch8dfjvpaoc5.us-east-1.rds.amazonaws.com',
  port: 3306,
  user: 'dbmasteruser',
  password: 'R6(bR3?KJq5[&[`yYACN(sI,Z9Yrwon6',
  database: 'ppScanningQA'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MySqlDataSourceDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'MySqlDataSource';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.MySqlDataSource', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
