import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 's3Bucket',
  connector: 'loopback-component-storage',
  provider: "amazon",
  key: "vz+PX5r0sPOerG2Ic3NS/YFiZ4bNF/thzTJyoqw0",
  keyId: "AKIAV3XFHJZRHTS2NV54",
  maxFileSize: "52428800"
};

// const config = {
//   name: 's3Bucket',
//   connector: 'loopback-component-storage',
//   provider: "amazon",
//   key: "hiBu8t3N84JIgPZsVWF65+B5cDYNDHUmmTc50shD",
//   keyId: "AKIATYIJB3T2JQ3DZBVO",
//   maxFileSize: "52428800"
// };

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class S3BucketDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 's3Bucket';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.s3Bucket', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
