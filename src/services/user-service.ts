// import {UserService } from '@loopback/authentication';
// import {Credentials, UserRepository} from '../repositories/user.repository';
// import {User} from '../models';
// import {repository} from '@loopback/repository';
// import {HttpErrors} from '@loopback/rest';
// import {inject} from '@loopback/core';
// import {BcryptHasher} from './hash.password.bcrypt';
// import {PasswordHasherBindings} from '../keys';
// import {MyUserProfile} from '../types';
// import {pick} from 'lodash';
// import {toJSON} from '@loopback/testlab';

// export class MyUserService implements UserService<User, Credentials> {
//   constructor(
//     @repository(UserRepository)
//     public userRepository: UserRepository,
//     @inject(PasswordHasherBindings.PASSWORD_HASHER)
//     public hasher: BcryptHasher,
//   ) {}
//   async verifyCredentials(credentials: Credentials): Promise<User> {
//     //
//     const foundUser:any = await this.userRepository.findOne({
//       where: {
//         email: credentials.email,
//       },
//     });
//     if (!foundUser) {
//       throw new HttpErrors.NotFound(
//         `user not found with this ${credentials.email}`,
//       );
//     }

//     const passwordMatched = await this.hasher.comparePassword(
//       credentials.password,
//       foundUser.password,
//     );
//     if (!passwordMatched) {
//       throw new HttpErrors.Unauthorized('password is not valid');
//     }
//     return foundUser;
//   }
//   convertToUserProfile(user: User): any {
//     let userName = '';
//     if (user.firstName) {
//       userName = user.firstName;
//     }
//     if (user.lastName) {
//       userName = user.firstName
//         ? `${user.firstName} ${user.lastName}`
//         : user.lastName;
//     }
//     const currentUser: MyUserProfile = pick(toJSON(user), [
//       'id',
//       'permissions',
//     ]) as MyUserProfile;
//     currentUser.name = userName;
//     return currentUser;
//   }
// }
