import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {MySqlDataSourceDataSource} from '../datasources';
import {Images, ImagesRelations, User} from '../models';
import {UserRepository} from './user.repository';
var moment = require('moment-timezone');

export class ImagesRepository extends DefaultCrudRepository<
  Images,
  typeof Images.prototype.id,
  ImagesRelations
> {

  public readonly user: BelongsToAccessor<User, typeof Images.prototype.id>;

  constructor(
    @inject('datasources.MySqlDataSource') dataSource: MySqlDataSourceDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Images, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);

  }


  definePersistedModel(entityClass: typeof Images) {
    const modelClass = super.definePersistedModel(entityClass);
    modelClass.observe('before save', async ctx => {
      // console.log(`going to save ${ctx.Model.modelName}`);
      if (ctx.instance && !ctx.instance.id) {
        ctx.instance.createdAt = moment().format('x');
      }
      if (ctx && ctx.data) {
        ctx.data.updatedAt = moment().format('x');
      }
    });
    return modelClass;
  }

}
