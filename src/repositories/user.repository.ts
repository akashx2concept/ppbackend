import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasOneRepositoryFactory, juggler, repository, BelongsToAccessor} from '@loopback/repository';
import {User, UserCredentials, UserRelations, Role} from '../models';
import {UserCredentialsRepository} from './user-credentials.repository';
import {RoleRepository} from './role.repository';

var moment = require('moment-timezone');


export class UserRepository extends DefaultCrudRepository<User, typeof User.prototype.id, UserRelations> {


  public userCredentials: HasOneRepositoryFactory<UserCredentials, typeof User.prototype.id>;

  public readonly role: BelongsToAccessor<Role, typeof User.prototype.id>;

  constructor(
    // @inject('datasources.mongo') dataSource: MongoDataSource,
    @inject('datasources.MySqlDataSource') dataSource: juggler.DataSource,
    @repository.getter('UserCredentialsRepository')
    protected userCredentialsRepositoryGetter: Getter<UserCredentialsRepository>, @repository.getter('RoleRepository') protected roleRepositoryGetter: Getter<RoleRepository>,
  ) {
    super(User, dataSource);
    this.role = this.createBelongsToAccessorFor('role', roleRepositoryGetter,);
    this.registerInclusionResolver('role', this.role.inclusionResolver);
    this.userCredentials = this.createHasOneRepositoryFactoryFor(
      'userCredentials',
      userCredentialsRepositoryGetter,
    );
  }


  definePersistedModel(entityClass: typeof User) {
    const modelClass = super.definePersistedModel(entityClass);
    modelClass.observe('before save', async ctx => {
      // console.log(`going to save ${ctx.Model.modelName}`);
      if (ctx.instance && !ctx.instance.id) {
        ctx.instance.createdAt = moment().format('x');
      }
      if (ctx && ctx.data) {
        ctx.data.updatedAt = moment().format('x');
      }
    });
    return modelClass;
  }

  async findCredentials(
    userId: typeof User.prototype.id,
  ): Promise<UserCredentials | undefined> {
    try {
      return await this.userCredentials(userId).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }

}


export type Credentials = {
  email: string;
  password: string;
};

export type RefreshToken = {
  AccessToken: string;
  RefreshToken: string;
  userId: string;
};

export type ChangePassword = {
  oldPassword: string;
  newPassword: string;
  cnfNewPassword: string;
};

export type RefreshGrant = {
  refreshToken: string;
};
