import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MySqlDataSourceDataSource} from '../datasources';
import {Role, RoleRelations} from '../models';
var moment = require('moment-timezone');

export class RoleRepository extends DefaultCrudRepository<Role, typeof Role.prototype.id, RoleRelations> {

  constructor(
    @inject('datasources.MySqlDataSource') dataSource: MySqlDataSourceDataSource,
  ) {
    super(Role, dataSource);
  }


  definePersistedModel(entityClass: typeof Role) {
    const modelClass = super.definePersistedModel(entityClass);
    modelClass.observe('before save', async ctx => {
      // console.log(`going to save ${ctx.Model.modelName}`);
      if (ctx.instance && !ctx.instance.id) {
        ctx.instance.createdAt = moment().format('x');
        ctx.instance.slug = await this.slugify(ctx.instance['name']);
        console.log(ctx.instance.slug);
      }
      if (ctx && ctx.data) {
        ctx.data.updatedAt = moment().format('x');
      }
    });
    return modelClass;
  }

  async slugify(string: string) {
    return string.toString().trim().toLowerCase().replace(/\s+/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
  }

}


