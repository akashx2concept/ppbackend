import {belongsTo, Entity, model, property} from '@loopback/repository';
import {User} from './user.model';

@model()
export class Images extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  url: string;

  @property({
    type: 'string',
  })
  originalName: string;

  @property({
    type: 'string',
  })
  displayName: string;

  @property({
    type: 'string',
  })
  type: string;

  @property({
    type: 'string',
  })
  docType: string;

  @property({
    type: 'string',
  })
  createdAt?: string;

  @property({
    type: 'string',
  })
  updatedAt?: string;

  @belongsTo(() => User)
  userId?: string;

  @property({
    type: 'string',
  })
  createdBy?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isFolder: boolean;

  @property({
    type: 'number',
  })
  isFolderId?: number;

  constructor(data?: Partial<Images>) {
    super(data);
  }
}

export interface ImagesRelations {
  // describe navigational properties here
}

export type ImagesWithRelations = Images & ImagesRelations;
