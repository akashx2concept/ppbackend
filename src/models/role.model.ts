import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: true,
    forceId: false,
    indexes: {
      uniqueSlug: {
        keys: {
          slug: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class Role extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    unique: true,
  })
  slug: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isDefault?: boolean;

  @property({
    type: 'boolean',
    default: true,
  })
  isActive?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isDeleted?: boolean;

  @property({
    type: 'string',
  })
  createdAt?: string;

  @property({
    type: 'string',
  })
  updatedAt?: string;

  constructor(data?: Partial<Role>) {
    super(data);
  }
}

export interface RoleRelations {
  // describe navigational properties here
}

export type RoleWithRelations = Role & RoleRelations;
