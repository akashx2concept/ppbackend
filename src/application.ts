import {AuthenticationComponent} from '@loopback/authentication';
import {JWTAuthenticationComponent, RefreshtokenService, RefreshTokenServiceBindings, TokenServiceBindings} from '@loopback/authentication-jwt';
import {AuthorizationComponent} from '@loopback/authorization';
import {BootMixin} from '@loopback/boot';
import {ApplicationConfig, BindingKey, createBindingFromClass} from '@loopback/core';
import {RepositoryMixin, SchemaMigrationOptions} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {RestExplorerBindings, RestExplorerComponent} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import crypto from 'crypto';
import {GlobalConfigBinding, PasswordHasherBindings, UserServiceBindings} from './keys';
import {ErrorHandlerMiddlewareProvider} from './middlewares';
import { RoleRepository } from './repositories';
import {MySequence} from './sequence';
import {BcryptHasher, JWTService, SecuritySpecEnhancer, UserManagementService} from './services';
import { GlobalConfigService } from './services/globalConfig.service';
import YAML = require('yaml');
import fs from 'fs';
import path from 'path';


/**
 * Information from package.json
 */
export interface PackageInfo {
  name: string;
  version: string;
  description: string;
}
export const PackageKey = BindingKey.create<PackageInfo>('application.package');

const pkg: PackageInfo = require('../package.json');

export {ApplicationConfig};

export class ScanningApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Bind authentication component related elements
    this.component(AuthenticationComponent);
    this.component(JWTAuthenticationComponent);
    this.component(AuthorizationComponent);

    this.setUpBindings();

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }


  setUpBindings(): void {
    // Bind package.json to the application context
    this.bind(PackageKey).to(pkg);

    // Bind bcrypt hash services
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);

    this.bind(UserServiceBindings.USER_SERVICE).toClass(UserManagementService);
    this.add(createBindingFromClass(SecuritySpecEnhancer));

    this.add(createBindingFromClass(ErrorHandlerMiddlewareProvider));

    // Use JWT secret from JWT_SECRET environment variable if set
    // otherwise create a random string of 64 hex digits
    // const secret = process.env.JWT_SECRET ?? crypto.randomBytes(32).toString('hex');
    this.bind(TokenServiceBindings.TOKEN_SECRET).to("aakash1147");
    // this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to("60");


    this.bind(RefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE).toClass(RefreshtokenService);
    this.bind(RefreshTokenServiceBindings.REFRESH_SECRET).to("aakash1147");
    this.bind(RefreshTokenServiceBindings.REFRESH_EXPIRES_IN).to("360000");
    this.bind(RefreshTokenServiceBindings.REFRESH_ISSUER).to("loopback4");

    //Global Config Service
    this.bind(GlobalConfigBinding.Global_Config).toClass(GlobalConfigService);

    this.bind('authentication.jwt.secretKey').to('aakash1147');
    this.bind('authentication.jwt.expiresIn').to('60 days');
  }

  // Unfortunately, TypeScript does not allow overriding methods inherited
  // from mapped types. https://github.com/microsoft/TypeScript/issues/38496
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  async start(): Promise<void> {
    // Use `databaseSeeding` flag to control if products/users should be pre
    // populated into the database. Its value is default to `true`.
    if (this.options.databaseSeeding !== false) {
      await this.migrateSchema();
    }
    return super.start();
  }

  async migrateSchema(options?: SchemaMigrationOptions): Promise<void> {
    await super.migrateSchema(options);

    // Pre-populate users
    const roleRepository = await this.getRepository(RoleRepository);
    // await userRepo.deleteAll();
    const roleDir = path.join(__dirname, '../fixtures/role');
    const roleFiles = fs.readdirSync(roleDir);

    for (const file of roleFiles) {
      if (file.endsWith('.yml')) {
        const roleFile = path.join(roleDir, file);
        const yamlString = YAML.parse(fs.readFileSync(roleFile, 'utf8'));
        let roleData = await roleRepository.findOne({ where: { id: yamlString['id'] } });
        if (!roleData) await roleRepository.create(yamlString);
      }
    }
  }



}
