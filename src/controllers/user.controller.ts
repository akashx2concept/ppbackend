// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {authenticate, TokenService, UserService} from '@loopback/authentication';
import {RefreshTokenService, RefreshTokenServiceBindings, TokenObject, TokenServiceBindings} from '@loopback/authentication-jwt';
import {authorize} from '@loopback/authorization';
import {inject} from '@loopback/core';
import { Count, CountSchema, Filter, FilterExcludingWhere, Where } from '@loopback/repository';
import {model, property, repository} from '@loopback/repository';
import {get, getModelSchemaRef, HttpErrors, param, post, put, requestBody, response} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';

import isemail from 'isemail';
import _ from 'lodash';
import {GlobalConfigBinding, PasswordHasherBindings, UserServiceBindings} from '../keys';
import {KeyAndPassword, ResetPasswordInit, User} from '../models';
import { ChangePassword, Credentials, RefreshToken, UserRepository, RefreshGrant, RoleRepository, ImagesRepository} from '../repositories';
import {basicAuthorization, GlobalConfigService, PasswordHasher, UserManagementService, validateCredentials, validateKeyPassword} from '../services';
import {OPERATION_SECURITY_SPEC} from '../utils';
import {ChangePasswordSchemaRequestBody, CredentialsRequestBody, PasswordResetRequestBody, RefreshGrantRequestBody, RefreshTokenRequestBody, UserProfileSchema} from './specs/user-controller.specs';

var moment = require('moment-timezone');

@model()
export class NewUserRequest extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}


export class UserController {

  constructor(
    @repository(UserRepository) public userRepository: UserRepository,
    @repository(RoleRepository) public roleRepository: RoleRepository,
    @repository(ImagesRepository) public imageRepository: ImagesRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public passwordHasher: PasswordHasher,
    @inject(TokenServiceBindings.TOKEN_SERVICE) public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE) public userService: UserService<User, Credentials>,
    @inject(UserServiceBindings.USER_SERVICE) public userManagementService: UserManagementService,
    @inject(RefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE) public refreshService: RefreshTokenService,
    @inject(GlobalConfigBinding.Global_Config) public globalConfigService: GlobalConfigService
    ) {}

  @get('/api/users')
  @response(200, {
    description: 'Array of User model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(User, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(User) filter?: Filter<User>,
  ): Promise<User[]> {
    return this.userRepository.find(filter);
  }

  @get('/api/users/count')
  @response(200, {
    description: 'Role model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.count(where);
  }

  
  @post('/api/signUp', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NewUserRequest, {
            title: 'NewUser',
          }),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ): Promise<User> {
    // All new users have the "customer" role by default
    let roleData:any = await this.roleRepository.findOne({ where: { slug: "super-admin" } });
    if (roleData) {
      newUserRequest.roleId = roleData['id'];
      // ensure a valid email value and password value
      validateCredentials(_.pick(newUserRequest, ['email', 'password']));
      try {
        newUserRequest.resetKey = '';
        return await this.userManagementService.createUser(newUserRequest);
      } catch (error) {
        console.log(error)
        // MongoError 11000 duplicate key
        if (error.code === 11000 && error.errmsg.includes('index: uniqueEmail')) {
          throw new HttpErrors.Conflict('Email value is already taken');
        } else {
          throw error;
        }
      }
    } else {
      throw new HttpErrors.NotFound('Role not found');
    }
  
  }

  @put('/api/users/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  // @authorize({
  //   allowedRoles: ['Root', 'admin', 'User'],
  //   voters: [basicAuthorization],
  // })
  async set(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.string('userId') userId: any,
    // @requestBody({description: 'update user'}) user: User,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    }) user: User,
  ): Promise<void> {
    try {
      // Only admin can assign roles
      if (currentUserProfile.roleId) { delete user.roleId; }
      return await this.userRepository.updateById(userId, user);
    } catch (e) {
      return e;
    }
  }

  @get('/api/users/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({
    allowedRoles: ['Root', 'admin', 'User'],
    voters: [basicAuthorization],
  })
  async findById(@param.path.string('userId') userId: any): Promise<User> {
    return this.userRepository.findById(userId);
  }

  @get('/api/users/me', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'The current user profile',
        content: {
          'application/json': {
            schema: UserProfileSchema,
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async printCurrentUser(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<any> {
    // (@jannyHou)FIXME: explore a way to generate OpenAPI schema
    // for symbol property

    const userId:any = currentUserProfile[securityId];
    
    return await this.userRepository.findOne({ where: {id: userId}, include: [{relation: "role"}] });
  }


  @post('/api/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{token: any}> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    // console.log(user);
    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);

    // create a JSON Web Token based on the user profile
    let token = await this.jwtService.generateToken(userProfile);

    return {token};
  }

  @put('/api/users/forgot-password', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'The updated user profile',
        content: {
          'application/json': {
            schema: UserProfileSchema,
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async forgotPassword(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody(PasswordResetRequestBody) credentials: Credentials,
  ): Promise<{token: string}> {
    const {email, password} = credentials;
    const {id} = currentUserProfile;

    const user = await this.userRepository.findById(id);

    if (!user) { throw new HttpErrors.NotFound('User account not found'); }
    if (email !== user?.email) { throw new HttpErrors.Forbidden('Invalid email address');}

    validateCredentials(_.pick(credentials, ['email', 'password']));
    const passwordHash = await this.passwordHasher.hashPassword(password);

    await this.userRepository.userCredentials(user.id).patch({password: passwordHash});
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);
    return {token};
  }

  @post('/api/users/reset-password/init', {
    responses: {
      '200': {
        description: 'Confirmation that reset password email has been sent',
      },
    },
  })
  async resetPasswordInit(
    @requestBody() resetPasswordInit: ResetPasswordInit,
  ): Promise<any> {
    if (!isemail.validate(resetPasswordInit.email)) {
      throw new HttpErrors.UnprocessableEntity('Invalid email address');
    }

    const nodeMailer = await this.userManagementService.requestPasswordReset(resetPasswordInit.email);

    if (nodeMailer['massage'] == 'success') {
      return { massage: 'Successfully sent reset password link'};
    }
    throw new HttpErrors.InternalServerError(
      'Error sending reset password email',
    );
  }

  @put('/api/users/reset-password/finish', {
    responses: {
      '200': {
        description: 'A successful password reset response',
      },
    },
  })
  async resetPasswordFinish(
    @requestBody() keyAndPassword: KeyAndPassword,
  ): Promise<string> {
    validateKeyPassword(keyAndPassword);

    const foundUser = await this.userRepository.findOne({
      where: {resetKey: keyAndPassword.resetKey},
    });

    if (!foundUser) {
      throw new HttpErrors.NotFound(
        'No associated account for the provided reset key',
      );
    }

    const user = await this.userManagementService.validateResetKeyLifeSpan(
      foundUser,
    );

    const passwordHash = await this.passwordHasher.hashPassword(
      keyAndPassword.password,
    );

    try {
      await this.userRepository
        .userCredentials(user.id)
        .patch({password: passwordHash});

      await this.userRepository.updateById(user.id, user);
    } catch (e) {
      return e;
    }

    return 'Password reset successful';
  }


  // @post('/refreshToken', {
  //   responses: {
  //     '200': {
  //       description: 'Token',
  //       content: {
  //         'application/json': {
  //           schema: {
  //             type: 'object',
  //             properties: {
  //               token: {
  //                 type: 'string',
  //               },
  //             },
  //           },
  //         },
  //       },
  //     },
  //   },
  // })
  // async refreshToken(@requestBody(RefreshTokenRequestBody) refreshToken: RefreshToken,): Promise<RefreshToken> {

  //   try {
  //     let tokenData:any = await this.accessTokenRepository.findOne({where: {  AccessToken: refreshToken['AccessToken'], RefreshToken: refreshToken['RefreshToken'], userId: refreshToken['userId'] }});
  //     if (tokenData) {
  //       const RefreshToken = refreshToken['RefreshToken']
  //       const RefreshToken_Verified = await this.userManagementService.verifyToken(RefreshToken);        
  //       const user = await this.userRepository.findById(refreshToken['userId']);
  //       const userProfile = this.userService.convertToUserProfile(user);
  //       let token = await this.jwtService.generateToken(userProfile);
  //       let refreshToekn = await this.userManagementService.generateRefreshToken(userProfile);
  //       let data:any = {
  //         id: tokenData['id'],
  //         AccessToken: token,
  //         RefreshToken: refreshToekn,
  //         userId: userProfile['id']
  //       }
  //       await this.accessTokenRepository.replaceById(data.id, data);
  //       const newToken:any = await this.accessTokenRepository.findById(data['id']);
  //       return newToken;
  //     } else {
  //       throw new HttpErrors.NotFound('Refresh Token not found',);
  //     }
  //   } catch (error) { 
  //     return error;
  //   }
  // }



  @post('/api/change-password', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async chnagePassword(
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
    @requestBody(ChangePasswordSchemaRequestBody) changePassword: ChangePassword): Promise<ChangePassword> {
    try {
      console.log(currentUserProfile);



      return changePassword;
    } catch (error) { 
      return error;
    }
  }

  @post('/api/users/refresh-login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                accessToken: {
                  type: 'string',
                },
                refreshToken: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async refreshLogin(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<TokenObject> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    // console.log(user);
    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile: UserProfile =this.userService.convertToUserProfile(user);
    const accessToken = await this.jwtService.generateToken(userProfile);
    const tokens = await this.refreshService.generateToken(userProfile, accessToken);
    return tokens;
  }



  @post('/api/refresh', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                accessToken: {
                  type: 'object',
                },
              },
            },
          },
        },
      },
    },
  })
  async refresh(
    @requestBody(RefreshGrantRequestBody) refreshGrant: RefreshGrant,
  ): Promise<TokenObject> {
    return this.refreshService.refreshToken(refreshGrant.refreshToken);
  }


  @post('/api/createUser', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async createUser(
    @requestBody({
      content: { 'application/json': {
          schema: getModelSchemaRef(NewUserRequest, {
            title: 'NewUser',
          }),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ): Promise<User> {
    // All new users have the "customer" role by default
    let roleData:any = await this.roleRepository.findOne({ where: { slug: "user" } });
    if (roleData) {
      newUserRequest.roleId = roleData['id'];
      // ensure a valid email value and password value
      validateCredentials(_.pick(newUserRequest, ['email', 'password']));
      try {
        newUserRequest.resetKey = '';
        let createdUser = await this.userManagementService.createUser(newUserRequest);

        let imageDataDto = [{
          url: `api/containers/${this.globalConfigService.S3Container}/download/folderImage.svg`,
          isFolder: true,
          displayName: "Bills",
          originalName: "Bills",
          createdBy: createdUser['id'],
          userId: createdUser['id']
        },{
          url: `api/containers/${this.globalConfigService.S3Container}/download/folderImage.svg`,
          isFolder: true,
          displayName: "Recipts",
          originalName: "Recipts",
          createdBy: createdUser['id'],
          userId: createdUser['id']
        }]
        await this.imageRepository.createAll(imageDataDto);

        return createdUser
      } catch (error) {
        console.log(error)
        // MongoError 11000 duplicate key
        if (error.code === 11000 && error.errmsg.includes('index: uniqueEmail')) {
          throw new HttpErrors.Conflict('Email value is already taken');
        } else {
          throw error;
        }
      }
    } else {
      throw new HttpErrors.NotFound('Role not found');
    }
  
  }




  @get('/api/users/dashboardAnalysis/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  // @authorize({
  //   allowedRoles: ['Root', 'admin', 'User'],
  //   voters: [basicAuthorization],
  // })
  async dashboardAnalysis(@param.path.string('userId') userId: any): Promise<any> {

    var lineActiveLabels = [];
    var lineInActiveData = [];
    var lineChartLabels = [];

    for (let i = 11; i >= 0; i--) {
      if (i == 0) {
        lineChartLabels.push(moment().format("MMM YYYY"));
        var month_dayStart = moment().startOf('month').format('x');
        var month_dayEnd = moment().endOf('day').format("x");

        let isActiveQuery:any = { isActive: true, createdBy: userId, createdAt: { between: [ month_dayStart, month_dayEnd ] }};
        var isActiveCount = await this.userRepository.count(isActiveQuery);
        lineActiveLabels.push(isActiveCount.count);

        let isInActiveQuery:any = { isActive: false, createdBy: userId, createdAt: { between: [ month_dayStart, month_dayEnd ] }};
        var isActiveCount = await this.userRepository.count(isInActiveQuery);
        lineInActiveData.push(isActiveCount.count);
      } else {
        lineChartLabels.push(moment().subtract(i, 'month').format("MMM YYYY"));
        var month_dayStart = moment(moment().subtract(i, 'month')).startOf('month').format("x");
        var month_dayEnd = moment(moment().subtract(i, 'month')).endOf('month').format("x");

        let isActiveQuery:any = { isActive: true, createdBy: userId, createdAt: { between: [ month_dayStart, month_dayEnd ] }};
        var isActiveCount = await this.userRepository.count(isActiveQuery);
        lineActiveLabels.push(isActiveCount.count);

        let isInActiveQuery:any = { isActive: false, createdBy: userId, createdAt: { between: [ month_dayStart, month_dayEnd ] }};
        var isActiveCount = await this.userRepository.count(isInActiveQuery);
        lineInActiveData.push(isActiveCount.count);

      }
    }

    let graphData = {
      lineActiveLabels: lineActiveLabels,
      lineInActiveData: lineInActiveData,
      lineChartLabels: lineChartLabels
    }

    return graphData;
    // return this.userRepository.findById(userId);
  }

}
