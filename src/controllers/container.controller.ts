import { inject } from '@loopback/core';
import { serviceProxy } from '@loopback/service-proxy';
import { authenticate } from '@loopback/authentication';
import { authorize} from '@loopback/authorization';
import { basicAuthorization } from '../services';
import { post, requestBody, del, param, get, getFilterSchemaFor, Request, Response, RestBindings } from '@loopback/rest';
import { Filter, repository } from '@loopback/repository';
import { promisify } from 'util';

import { IStorageService } from '../interfaces';
import { Container, File, Images } from '../models';
import { GlobalConfigBinding } from '../keys';
import { GlobalConfigService } from '../services';
import { ImagesRepository } from '../repositories';


export class StorageGcController {
    @inject('services.StorageGCService')
    private storageGcSvc: IStorageService;

  constructor(
      @repository(ImagesRepository) public imagesRepository: ImagesRepository,
      @inject(RestBindings.Http.REQUEST) public request: Request,
      @inject(RestBindings.Http.RESPONSE) public response: Response,
      @inject(GlobalConfigBinding.Global_Config) public globalConfigService: GlobalConfigService,
    ) {

    }

//   @post('/containers', {
//     responses: {
//       '200': {
//         description: 'Container model instance',
//         content: { 'application/json': { schema: { 'x-ts-type': Container } } },
//       },
//     },
//   })
//   async createContainer(@requestBody() container: Container): Promise<Container> {
//     const createContainer = promisify(this.storageGcSvc.createContainer);
//     return await createContainer(container);
//   }

//   @get('/containers', {
//     responses: {
//       '200': {
//         description: 'Array of Containers model instances',
//         content: {
//           'application/json': {
//             schema: { type: 'array', items: { 'x-ts-type': Container } },
//           },
//         },
//       },
//     },
//   })
//   async findContainer(@param.query.object('filter', getFilterSchemaFor(Container)) filter?: Filter): Promise<Container[]> {
//     const getContainers = promisify(this.storageGcSvc.getContainers);
//     return await getContainers();
//   }

//   @get('/containers/{containerName}', {
//     responses: {
//       '200': {
//         description: 'Container model instance',
//         content: { 'application/json': { schema: { 'x-ts-type': Container } } },
//       },
//     },
//   })
//   async findContainerByName(@param.path.string('containerName') containerName: string): Promise<Container> {
//     const getContainer = promisify(this.storageGcSvc.getContainer);
//     return await getContainer(containerName);
//   }

//   @del('/containers/{containerName}', {
//     responses: {
//       '204': {
//         description: 'Container DELETE success',
//       },
//     },
//   })
//   async deleteContainerByName(@param.path.string('containerName') containerName: string): Promise<boolean> {
//     const destroyContainer = promisify(this.storageGcSvc.destroyContainer);
//     return await destroyContainer(containerName);
//   }

//   @get('/containers/{containerName}/files', {
//     responses: {
//       '200': {
//         description: 'Array of Files model instances belongs to container',
//         content: {
//           'application/json': {
//             schema: { type: 'array', items: { 'x-ts-type': File } },
//           },
//         },
//       },
//     },
//   })
//   async findFilesInContainer(@param.path.string('containerName') containerName: string,
//     @param.query.object('filter', getFilterSchemaFor(Container)) filter?: Filter): Promise<File[]> {
//     const getFiles = promisify(this.storageGcSvc.getFiles);
//     return await getFiles(containerName, {});
//   }

  // @get('/containers/{containerName}/files/{fileName}', {
  //   responses: {
  //     '200': {
  //       description: 'File model instances belongs to container',
  //       content: { 'application/json': { schema: { 'x-ts-type': File } } },
  //     },
  //   },
  // })
  // @authenticate('jwt')
  // @authorize({
  //   allowedRoles: ['Root', 'admin', 'User'],
  //   voters: [basicAuthorization],
  // })
  // async findFileInContainer(@param.path.string('containerName') containerName: string,
  //   @param.path.string('fileName') fileName: string): Promise<File> {
  //   const getFile = promisify(this.storageGcSvc.getFile);
  //   return await getFile(this.globalConfigService.S3Container, fileName);
  // }

  @del('/api/containers/{containerName}/files/{fileName}', {
    responses: {
      '204': {
        description: 'File DELETE from Container success',
      },
    },
  })
  @authenticate('jwt')
  // @authorize({
  //   allowedRoles: ['Root', 'admin', 'User'],
  //   voters: [basicAuthorization],
  // })
  async deleteFileInContainer(@param.path.string('containerName') containerName: string,
    @param.path.string('fileName') fileName: string): Promise<boolean> {
    const removeFile = promisify(this.storageGcSvc.removeFile);
    return await removeFile(this.globalConfigService.S3Container, fileName);
  }

  @post('/api/containers/{containerName}/upload', {
    responses: {
      '200': {
        description: 'Upload a Files model instances into Container',
        content: { 'application/json': { schema: { 'x-ts-type': Images } } },
      },
    },
  })
  @authenticate('jwt')
  // @authorize({
  //   allowedRoles: ['Root', 'admin', 'User'],
  //   voters: [basicAuthorization],
  // })
  async upload(@param.path.string('containerName') containerName: string): Promise<any> {
    const upload = promisify(this.storageGcSvc.upload);
    let uploadDoc = await upload(this.globalConfigService.S3Container, this.request, this.response, {});
    let CONTAINER_URL = this.globalConfigService.S3Container;
    var fileInfoArr = uploadDoc.files.file;
    // var objs = {};
    // fileInfoArr.forEach(function(item:any) {
    //     objs = {
    //       originalName: item.name,
    //       displayName: item.name,
    //       type: item.type,
    //       url: `containers/${CONTAINER_URL}/download/${item.name}`,
    //     };
    // });    
    // return await this.imagesRepository.create(objs);
    return {url: `api/containers/${CONTAINER_URL}/download/${fileInfoArr[0].name}`};
  }

  @get(`/api/containers/{containerName}/download/{fileName}`, {
    responses: {
      '200': {
        description: 'Download a File within specified Container',
        content: { 'application/json': { schema: { 'x-ts-type': Object } } },
      },
    },
  })
  // @authenticate('jwt')
  // @authorize({
  //   allowedRoles: ['Root', 'admin', 'User'],
  //   voters: [basicAuthorization],
  // })
  async download(@param.path.string('containerName') containerName: string,
    @param.path.string('fileName') fileName: string): Promise<any> {
    const download = promisify(this.storageGcSvc.download);
    return await download(this.globalConfigService.S3Container, fileName, this.request, this.response);
  }
}