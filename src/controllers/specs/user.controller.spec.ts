import { User } from "../../models";
import {Entity, Model, model, property} from '@loopback/repository';

@model()
export class LoginSchema extends Model {
  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

export const LoginRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {'application/json': {schema: {'x-ts-type': LoginSchema}}},
};
