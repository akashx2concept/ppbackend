// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {SchemaObject} from '@loopback/rest';

// TODO(jannyHou): This should be moved to @loopback/authentication
export const UserProfileSchema = {
  type: 'object',
  required: ['id'],
  properties: {
    id: {type: 'string'},
    email: {type: 'string'},
    name: {type: 'string'},
    roleId: {type: 'number'},
  },
};

// TODO(jannyHou): This is a workaround to manually
// describe the request body of 'Users/login'.
// We should either create a Credential model, or
// infer the spec from User model

const CredentialsSchema: SchemaObject = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};

export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};

export const PasswordResetRequestBody = {
  description: 'The input of password reset function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};



const RefreshTokenSchema: SchemaObject = {
  type: 'object',
  required: ['AccessToken', 'RefreshToken', 'userId'],
  properties: {
    AccessToken: {
      type: 'string',
    },
    RefreshToken: {
      type: 'string',
    },
    userId: {
      type: 'string'
    }
  },
};

export const RefreshTokenRequestBody = {
  description: 'The input of Refresh Token Schema',
  required: true,
  content: {
    'application/json': {schema: RefreshTokenSchema},
  },
};


const ChangePasswordSchema: SchemaObject = {
  type: 'object',
  required: ['oldPassword', 'newPassword', 'cnfNewPassword'],
  properties: {
    oldPassword: {
      type: 'string',
      minLength: 8,
    },
    newPassword: {
      type: 'string',
      minLength: 8,
    },
    cnfNewPassword: {
      type: 'string',
      minLength: 8,
    },
  },
};

export const ChangePasswordSchemaRequestBody = {
  description: 'The input of Refresh Token Schema',
  required: true,
  content: {
    'application/json': {schema: ChangePasswordSchema},
  },
};


// Describes the schema of grant object
const RefreshGrantSchema: SchemaObject = {
  type: 'object',
  required: ['refreshToken'],
  properties: {
    refreshToken: {
      type: 'string',
    },
  },
};

// Describes the request body of grant object
export const RefreshGrantRequestBody = {
  description: 'Reissuing Acess Token',
  required: true,
  content: {
    'application/json': {schema: RefreshGrantSchema},
  },
};
